/*
    Les tableaux :
    Découvrez comment manipuler des tableaux.
    Vous trouverez dans la documentation des méthodes utiles
    pour réaliser les objectifs décrits après.
    Prenez votre temps pour lire la description et compendre l'utilisation des méthodes
    et n'hésitez pas à vous entraider.

    Lorsque je vous demande d'afficher une donnée, il faut l'afficher dans la console.

    Conseil : n'hésitez pas à tester en ajoutant ou modifiant la valeur des variables.

    Documentation : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array
 */

/**
 * Exercice 1 :
 * Créez un tableau vide dans une variable ou constante
 */
let arr;
console.log(arr);

/**
 * Exercice 2
 * Créez un tableau dans une constante avec des valeurs initiales : "orange", "red", "pink", "blue"
 */
const arr2 = ["orange", "red", "pink", "blue"];

/**
 * Exercice 3
 * Créez un tableau dans une variable avec un maximum de 10 entrées et remplie du boolean "false"
 */

const arr3= new Array(arraylength=10).fill(false);
/**
 * Exercice 4
 * Créez un tableau dans une variable et ajoutez-y plusieurs valeurs de votre choix
 */
let arr4 = [1,"nesrine","samy",3 , true ];

/**
 * Exercice 5
 * Créez un tableau dans une variable et affichez la deuxième valeur dans la console
 */
let arr5 = ["kamel","samy","nesrine"];

console.log("Position :", arr5, arr5[1]);
/**
 * Exercice 6
 * Créez un tableau dans une variable, ajoutez-y 10 éléments de votre choix et supprimez la deuxième valeur
 */
let arr6 =["nesrine","kamel","samy","sihem","sara","lisa","nadia","camil","rayane","hana"];

arr6.splice(1,deletecount=1);//Suppresion de kamel correspondant à l'index n°1

console.log("ex 6 :", arr6);
/**
 * Exercice 7 :
 * Créez un tableau dans une variable et ajoutez-y 10 éléments de votre choix et supprimez la première valeur
 */
let arr7 = ["nesrine","kamel","samy","sihem","sara","lisa","nadia","camil","rayane","hana"];

//arr7.splice(1,1);

arr7.shift();

console.log("ex 7 :", arr7);

/**
 * Exercice 8 :
 * Créez un tableau dans une variable et ajoutez-y 10 éléments de votre choix et supprimez la dernière valeur
 */

let arr8 = ["nesrine","kamel","samy","sihem","sara","lisa","nadia","camil","rayane","hana"];
//arr8.splice(9,1);
//arr8.splice(arr8.length - 1,1);// si on connait pas la longeur du tableur
arr8.pop();

console.log("exe 8:", arr8);
/**
 * Exercice 9 :
 * Créez un tableau dans une variable nommée "mat" contenant 2 tableaux vide
 * Ajoutez dans le premier tableau du tableau "mat" le nombre 0
 * Ajoutez dans le deuxième tableau du tableau "mat" le nombre 1
 * Ajoutez dans le deuxième tableau du tableau "mat" le nombre 2
 * Ajoutez dans le premier tableau du tableau "mat" le nombre 3
 * Affichez le résultat dans la console
 */

let mat = [[],[]];

mat[0].push(0);
mat[1].push(1);
mat[1].push(2);
mat[0].push(3);

console.log("exe 9 :" ,mat);



/**
 * Exercice 10 : Bonus "Immutabilité"
 * Recherchez et décrivez ce qu'est "l'immutabilité"
 * Pourquoi il faut faire attention à l'immutabilité lorsqu'on manipule des tableaux
 */

let arr10 = [2];
let arr11 = arr10;// copie la referance du tableau arr10

let arr12= arr11;
let arr13= [...arr12]; // on va etaler la variable 12 (spread out = etaler )
// on copie les valeur dans un autre tableau
// créer un nouveau tableu
console.log(arr10,arr11);

arr10.shift() //suprimer la valeur


//let arr = [...le tableau] la santtaxe