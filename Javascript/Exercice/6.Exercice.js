// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Object

/**
 * Exercice 1 :
 * Assignez à une variable "user" un objet qui a pour propriété âge, name et notes (un tableau de 8 nombres)
 */
let user ={
  age : 31,
  name: 'nesrine',
  notes : [12,13,10,14,15,6,7,9],

}
/**
 * Exercice 2 :
 * Affichez dans la console le nom, l'âge, et les notes de l'utilisateur "user"
 */
console.log('ex 2 :', "nom :", user.name );
console.log('ex 2 :', "age :", user.age );
console.log('ex 2 :', "les notes obtenus :",user.notes );
/**
 * Exercice 3 :
 * Affichez toutes les propriétés de l'objet "user" dans un tableau
 * Effectuer une recherche sur intenet pour trouver la solution
 */

console.log('ex 3 :', "Les propriétés de l'objet dans un tableau :", Object.keys(user) );


/**
 * Exercice 4 :
 * Ajoutez une nouvelle propriété à l'objet "user"
 * la "clé" de cette nouvelle propriété est "countNotes"
 * la valeur de "countNotes" est le nombre de notes que possèdes l'utilisateurs
 */


user.countNotes = user.notes.length;

console.log("ex 4 : Le nombre de note de ",user.name, "est ",user.countNotes);

/**
 * Exercice 5 :
 * Déterminez la note moyenne de l'utilisateur "user"
 * Utilisez une boucle pour boucler au travers du tableau de notes de l'obejt "user"
 * et additionnez chaque note entre elle puis diviser la somme totale
 * des notes par le nombre de notes pour connaître la noteMoyenne
 */
let noteMoyenne;
let somme=0;

for (let i=0 ; i<user.notes.length ; i++)
    {
    somme=somme+user.notes[i];
}

noteMoyenne=somme/user.notes.length;

console.log("la moyenne :",noteMoyenne );


/**
 * Exercice 6 :
 * Ajoutez une nouvelle propriété à l'objet "user"
 * la "clé" de cette nouvelle propriété est "moyenne"
 * la valeur de "moyenne" est la valeur de la variable "noteMoyenne"
 */

user.moyenne=noteMoyenne;


console.log("Ex 6:",user);

/**
 * Exercice 7 :
 * Ajoutez une nouvelle méthode à l'objet "user"
 * le nom de cette méthode est "myName"
 * Cette méthode affiche dans la console le nom de l'utilisateur
 * Invoquez la méthode "myName" de l'objet "user"
 */

console.log("exercice 7: " ,user);


user.myName=function (){
    console.log("ex 7 bis:",this.name);
}

user.myName();







/**
 * Exercice 8 :
 * Ajoutez une nouvelle méthode à l'objet "user"
 * le nom de cette méthode est "addNote"
 * Cette méthode ajoute une nouvelle note dans le tableau de notes de l'objet courant
 * La méthode "addNote" prend 1 argument, un nombre dont la valeur par défault est 10
 * Invoquez la méthode "addNote" de l'objet "user" 2 fois, une première fois sans argument
 * et une seconde fois avec un nombre de votre choix
 */



user.addNote=function (nb=10){
    this.notes.push(nb)
}

user.addNote();
user.addNote(12);
user.addNote(1);





/**
 * Exercice 9 :
 * Affichez dans la console les notes de l'objet "user"
 */
console.log("ex 10:",user.notes);
/**
 * Exercice 10 : BONUS
 * Ajoutez une nouvelle méthode à l'objet "user"
 * le nom de cette méthode est "calculeLaMoyenne"
 * Cette méthode doit calculer la moyenne de l'utilisateur courant et
 * enregistre la moyenne calculée dans la clé "moyenne" de l'objet courant
 * Elle ne prend aucun argument.
 * Dans la méthode "calculeLaMoyenne" vous devez donc reproduire l'exercice 5.
 *
 * Invoquez la méthode "calculeLaMoyenne" de l'objet "user"
 * Affichez dans la console la nouvelle moyenne de l'utilisateur "user"
 */

user.calculeLaMoyenne= function (){

    let cumul = 0;
    let moy2;

    for (let i = 0; i < user.notes.length; i++) {
        cumul=cumul+user.notes[i];
    }
    moy2=cumul/this.notes.length;
    console.log("moyenne 2", moy2);
    this.calculeLaMoyenne=moy2;
}

user.calculeLaMoyenne();

console.log("Bonus :",user.calculeLaMoyenne);



